**BDD Cucumber with Page Object Design pattern**

Due to time constraints, I opted for the BDD Cucumber framework with built-in Cucumber reporting instead of the more comprehensive Serenity reporting. Leveraging my prior experience with BDD using Cucumber, this choice allowed me to efficiently align the framework's standardization and structure with the assignment's time constraints.

List of tools used to build framework:

Tech stack used: 
- Programming Language: Java
- Framework: BDD Cucumber
- Unit Testing: Junit
- UI Automation Tool: Selenium,WebdriverManager,
- Build Tool: Maven,
- IDE: Intellij,
- CI platform : Gitlab

Framework structure
![Framework Structure](images/FrameworkStructure.png)
- **src/main/java/com/swisscom/uiautomation/actions**: **actions** package has optimised page methods for step definitions 
- **src/main/java/com/swisscom/uiautomation/pages**: **pages** package contains page locators and methods 
- **src/main/java/com/swisscom/uiautomation/utils**: **utils** pacakage contains common helper methods, setup and teardown the browser methods and also takes screenshot if scenarios fails and tear down the browse safely.
- **src/test/java/com/swisscom/uiautomation/definitions**: **defintions** package contains respective page step definitions for respective steps in the feature file
- **src/test/java/com/swisscom/uiautomation/runner**: **runner** package contains runner java class where execution starts and links step definitions and feature files 
- **src/test/resources/features/AddProductToTheCart.feature** : Test case is written in gherkin language
- **pom.xml** : **pom.xml** file contains all the dependency files required to run the project
- **cucumber-html-report.html**: This is the cucumber in built report stored under target root folder

**How to write test cases ?**

- Create a new feature file under features folder
- You can add input data under scenario / scenario outline based on the requirement.
- If you want to report in json/html/xml, update the plugin option in runner class -  for eg: plugin = {"pretty", "summary", "html:target/cucumber-html-report.html"},
- Test case is written in Gherkin language. So add step definition under definition folder with tag @Given @When @And @Then
- Action classes contains all respective page actions and assertions
- Page classes contains static and dynamic locators, page actions and constants.

**How to execute test cases ?**

- Preconditions: clone the repository:- https://github.com/sathyarajr1991new/SwisscomProject.git
- Execute from IDE: right click on CucumberRunnerTests.java , Run as Junit Test
- Execute from MAVEN - View>Tool Windows>Maven, right click on Maven icon in side tool bar, click on clean and then verify
- Execute from command prompt 
 $ mvn clean install
- GitLab CI all cases gets executed by default after each commit

**Console logs During execution**
![console_ouput](images/console_ouput.png)

**Cucumber html report in target folder**
![Cucumber Report](images/CucumberReport1.png)

