package com.swisscom.uiautomation.pages;

import com.swisscom.uiautomation.utils.CommonMethods;
import com.swisscom.uiautomation.utils.HelperClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * This class contains locators and methods for interacting with elements on the home page.
 */
public class HomePage extends CommonMethods {

    WebDriver driver;

    protected HomePage() {
        waitForPageToLoad();
        this.driver = HelperClass.getDriver();
    }

    /**
     * This section contains dynamic locator for home page.
     */
    public String navigationLink = "a[aria-label*='%s']";

    /**
     * This section contains locator for home page.
     */
    protected By shadowHeaderBlock = By.cssSelector("sdx-header");

    protected By swisscomHomeLogo = By.cssSelector("a[aria-label='swisscom home']");

    protected By homePageConsentWindow = By.cssSelector("button#onetrust-accept-btn-handler");


    /**
     * Handles the consent window if it is displayed.
     *
     * @throws Exception if an error occurs while handling the consent window
     */
    protected void handleConsentWindow() throws Exception {
        boolean isDisplayed = waitForVisibilityOfBySelector(homePageConsentWindow);
        if (isDisplayed) {
            clickOnElementUsingActions(driver.findElement(homePageConsentWindow));
        }
    }

    /**
     * Checks if the user is on the home page by opening the specified URL and verifying the presence of the Swisscom logo.
     *
     * @param url The URL of the home page.
     * @return true if the user is on the home page, otherwise false
     * @throws Exception if an error occurs while navigating to the home page or verifying the presence of the logo
     */
    protected boolean isUserOnHomePage(String url) throws Exception {
        HelperClass.openPage(url);
        handleConsentWindow();
        waitForVisibilityOfElement(driver.findElement(shadowHeaderBlock).getShadowRoot().findElement(swisscomHomeLogo));
        return returnShadowElement(shadowHeaderBlock).findElement(swisscomHomeLogo).isDisplayed();
    }

    /**
     * Clicks on the navigation tab with the specified label.
     *
     * @param navigationTab The label of the navigation tab to click.
     * @throws Exception if an error occurs while clicking on the navigation tab
     */
    protected void clickOnNavigationTab(String navigationTab) throws Exception {
        By updatedNavigationTab = By.cssSelector(String.format(navigationLink, navigationTab));
        waitForVisibilityOfElement(driver.findElement(shadowHeaderBlock).getShadowRoot().findElement(updatedNavigationTab));
        returnShadowElement(shadowHeaderBlock).findElement(updatedNavigationTab).click();
    }

    /**
     * Clicks on the product with the specified label.
     *
     * @param productionSelection The label of the product to click.
     * @throws Exception if an error occurs while clicking on the product
     */
    protected void clickOnProduct(String productionSelection) throws Exception {
        By updatedNavigationLink = By.cssSelector(String.format(navigationLink, productionSelection));
        waitForVisibilityOfElement(driver.findElement(shadowHeaderBlock).getShadowRoot().findElement(updatedNavigationLink));
        returnShadowElement(shadowHeaderBlock).findElement(updatedNavigationLink).click();

    }

    /**
     * Clicks on the sub-product with the specified label.
     *
     * @param subProductSelection The label of the sub-product to click.
     * @throws Exception if an error occurs while clicking on the sub-product
     */
    protected void clickOnSubProduct(String subProductSelection) throws Exception {
        By updatedProductCategorySelection = By.cssSelector(String.format(navigationLink, subProductSelection));
        waitForVisibilityOfElement(driver.findElement(shadowHeaderBlock).getShadowRoot().findElement(updatedProductCategorySelection));
        returnShadowElement(shadowHeaderBlock).findElement(updatedProductCategorySelection).click();
    }
}
