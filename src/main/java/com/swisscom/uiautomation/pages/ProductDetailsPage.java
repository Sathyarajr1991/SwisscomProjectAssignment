package com.swisscom.uiautomation.pages;

import com.swisscom.uiautomation.utils.CommonMethods;
import com.swisscom.uiautomation.utils.Context;
import com.swisscom.uiautomation.utils.HelperClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * This class contains locators for elements on the product details page.
 */
public class ProductDetailsPage extends CommonMethods {

    WebDriver driver;

    public static final String NOTIFICATION_TEXT = "Your product has been added to the shopping cart";

    /**
     * Constructs a new ProductDetailsPage object, initializes WebDriver, and waits for the page to load.
     */
    protected ProductDetailsPage() {
        waitForPageToLoad();
        driver = HelperClass.getDriver();
    }

    /**
     * Dynamic locator for the product details page.
     */
    public String productTitle = "//h1[normalize-space()='%s']";


    /**
     * Locators for elements on the product details page.
     */
    public By notificationShadowWindow = By.cssSelector("sdx-backdrop>sdx-dialog-content");

    public By notificationProductAddedText = By.cssSelector("h1#sdx-dialog-content-label");
    public By goToCartButtonInDetailsPage = By.cssSelector("sdx-button[data-cy='progressive-step-next-button']");

    public By continueShoppingButton = By.cssSelector("sdx-button-group>sdx-button[aria-label='Continue shopping']");

    /**
     * Checks if the user has landed on the product details page by verifying the presence of the expected product title.
     *
     * @return true if the user has successfully landed on the product details page, otherwise false
     * @throws Exception if an error occurs while locating the product title or waiting for its visibility
     */
    public boolean isUserLandOnProductDetailsPage() throws Exception {
        String expectedProductName = getValueFromMap(String.valueOf(Context.PRODUCT_NAME));
        By updatedProductTitle = By.xpath(String.format(productTitle, expectedProductName));
        return waitForVisibilityOfBySelector(updatedProductTitle);
    }


    /**
     * Checks if the user has successfully added an item to the cart.
     *
     * @return The notification message indicating the success of adding the item to the cart
     * @throws Exception if an error occurs while checking the notification or navigating
     */
    public String isUserAddedItemToTheCart() throws Exception {
        waitForElementToBeClickable(driver.findElement(goToCartButtonInDetailsPage));
        clickOnElementUsingJs(driver.findElement(goToCartButtonInDetailsPage));
        waitForVisibilityOfBySelector(notificationShadowWindow);
        String actualNotificationValue = returnTextFromTheShadowElement(notificationShadowWindow, notificationProductAddedText);
        clickOnElementBySelectorUsingJs(continueShoppingButton);
        return actualNotificationValue;
    }

}
