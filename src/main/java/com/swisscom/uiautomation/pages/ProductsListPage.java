package com.swisscom.uiautomation.pages;

import com.swisscom.uiautomation.utils.CommonMethods;
import com.swisscom.uiautomation.utils.Context;
import com.swisscom.uiautomation.utils.HelperClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * This class contains locators and methods for interacting with elements on the product list page.
 */
public class ProductsListPage extends CommonMethods {

    WebDriver driver;

    /**
     * Constructs a new ProductsListPage object, initializes WebDriver, and waits for the page to load.
     */
    protected ProductsListPage() {
        waitForPageToLoad();
        this.driver = HelperClass.getDriver();
    }

    /**
     * Locator for the dynamic elements on product list page
     */
    public String productListPage = "//h1[normalize-space()='%s']";
    public String filterTypeSelection = "//sdx-select[@label='%s']";

    public String filterTypeOptionSelection = "//sdx-select-option[@data-cy-select-opt-value='%s']";

    /**
     * Locators for elements on the product list page.
     */
    public By filterArrowExpander = By.xpath("//sdx-select[@aria-expanded='true']");

    public By filterRootShadowSection = By.cssSelector("ui-kit-filter-list>sdx-filter");

    public By filterSubRootShadowSection = By.cssSelector("sdx-scroll-view");

    public By filterSelectedRootShadow = By.cssSelector("sdx-button");

    public By filterTypeSelectedName = By.cssSelector("div>button>span");

    public By productLists = By.cssSelector("sdx-teaser");

    public By productText = By.cssSelector("a>div.second>div:nth-child(2)");


    /**
     * Checks if the user has landed on the product list page by verifying the visibility of the specified product list title.
     *
     * @param productListTitleName The name of the product list title to verify.
     * @return true if the user has successfully landed on the product list page, otherwise false
     * @throws Exception if an error occurs while locating the product list title or waiting for its visibility
     */
    public boolean isUserLandOnProductListPage(String productListTitleName) throws Exception {
        // Construct the locator for the product list title using the provided name
        By updatedProductListTitle = By.xpath(locatorUpdate(productListPage, productListTitleName));

        // Wait for the product list title to be visible on the page
        return waitForVisibilityOfBySelector(updatedProductListTitle);
    }

    /**
     * Clicks on the specified filter type on the product list page.
     *
     * @param filterType The type of filter to click.
     * @throws Exception if an error occurs while locating or clicking the filter type
     */
    public void clickOnFilterType(String filterType) throws Exception {
        waitForPageToLoad();
        String updatedFilterType = locatorUpdate(filterTypeSelection, filterType);
        waitForElementToBeClickable(driver.findElement(By.xpath(updatedFilterType)));
        clickOnElementUsingActions(driver.findElement(By.xpath(updatedFilterType)));
    }

    /**
     * Clicks on the specified filter type option on the product list page.
     *
     * @param filterTypeOptionType The type of filter option to click.
     * @throws Exception if an error occurs while locating or clicking the filter type option
     */
    public void clickOnFilterTypeOption(String filterTypeOptionType) throws Exception {
        String updatedFilterTypeSelectorOption = locatorUpdate(filterTypeOptionSelection, filterTypeOptionType);
        waitForElementToBeClickable(driver.findElement(By.xpath(updatedFilterTypeSelectorOption)));
        clickOnElementUsingJs(driver.findElement(By.xpath(updatedFilterTypeSelectorOption)));
        clickOnElementUsingActions(driver.findElement(filterArrowExpander));
    }

    /**
     * Checks if the specified filter selected text is found on the product list page.
     *
     * @param expFilterSelectedText The expected filter selected text.
     * @return true if the specified filter selected text is found, otherwise false
     * @throws Exception if an error occurs while locating or retrieving the filter selected text
     */
    public boolean isFilterSelectedTextFound(String expFilterSelectedText) throws Exception {
        // Wait for the filter selected text to be found and compare it with the expected text
        waitForVisibilityOfElement(returnShadowElement(filterRootShadowSection)
                .findElement(filterSubRootShadowSection));
        List<WebElement> rootFilterSelectedList = returnShadowElement(filterRootShadowSection)
                .findElement(filterSubRootShadowSection)
                .findElements(filterSelectedRootShadow);
        for (WebElement element : rootFilterSelectedList) {
            String actualFilterSelectedText = element.getShadowRoot().findElement(filterTypeSelectedName).getText();
            if (actualFilterSelectedText.equalsIgnoreCase(expFilterSelectedText)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the last product element from the product list page.
     *
     * @return The last product element on the page.
     * @throws Exception if an error occurs while retrieving the last product element
     */
    public WebElement returnLastProductElement() throws Exception {
        // Wait for the product lists to be visible and return the last product element
        waitForVisibilityOfBySelector(productLists);
        List<WebElement> productList = getElementLists(productLists);
        return productList.get(productList.size() - 1);
    }

    /**
     * Stores the name of the last product displayed on the product list page in the context.
     *
     * @throws Exception if an error occurs while retrieving or storing the last product name
     */
    public void storeTheLastProductName() throws Exception {
        // Retrieve the last product element, get its name, and store it in the context
        WebElement lastElement = returnLastProductElement();
        javaScriptScrollToViewElement(lastElement);
        String productName = lastElement.getShadowRoot().findElement(productText).getText();
        setValueToMap(String.valueOf(Context.PRODUCT_NAME), productName);
        // Click on the last product element to view its details
        clickOnElementUsingActions(lastElement);
    }

}
