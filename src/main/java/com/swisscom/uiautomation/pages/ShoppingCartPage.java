package com.swisscom.uiautomation.pages;

import com.swisscom.uiautomation.utils.CommonMethods;
import com.swisscom.uiautomation.utils.Context;
import com.swisscom.uiautomation.utils.HelperClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * This class contains locators and methods for interacting with the shopping cart page.
 */
public class ShoppingCartPage extends CommonMethods {

    WebDriver driver;

    /**
     * Constructs a new ShoppingCartPage object, initializes WebDriver, and waits for the page to load.
     */
    protected ShoppingCartPage() {
        waitForPageToLoad();
        this.driver = HelperClass.getDriver();
    }

    /**
     * Locators for elements on the shopping cart page.
     */
    protected By shadowHeaderBlock = By.cssSelector("sdx-header");

    protected By shoppingCartTitle = By.cssSelector("div.summary-title");

    protected By productNameInTheCart = By.cssSelector("div.product-name");
    protected By cartTrayIcon = By.cssSelector("a[aria-label='icon-shopping-trolley']");


    /**
     * Checks whether the user has landed on the shopping cart page.
     *
     * @return true if the shopping cart page is visible, false otherwise
     * @throws Exception if an error occurs while verifying the visibility of the shopping cart page
     */
    protected boolean isUserLandCartPage() throws Exception {
        return waitForVisibilityOfElement(driver.findElement(shoppingCartTitle));
    }

    /**
     * Clicks on the shopping cart icon.
     *
     * @throws Exception if an error occurs while clicking on the cart icon
     */
    protected void clickOnCartTrolleyIcon() throws Exception {
        waitForVisibilityOfBySelector(shadowHeaderBlock);
        returnShadowElement(shadowHeaderBlock).findElement(cartTrayIcon).click();
    }

    /**
     * Retrieves the text of the stored product from the shopping cart.
     *
     * @return The text of the stored product in the cart
     * @throws Exception if an error occurs while retrieving the product text
     */
    public String returnStoredProductText() throws Exception {
        waitForVisibilityOfBySelector(productNameInTheCart);
        return getValueFromMap(String.valueOf(Context.PRODUCT_NAME));
    }
}
