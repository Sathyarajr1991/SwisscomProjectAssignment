/**
 * This abstract class provides common utility methods for interacting with web elements and managing WebDriver instances.
 */
package com.swisscom.uiautomation.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommonMethods {

    /**
     * The default timeout duration in seconds for WebDriver wait.
     */
    public final int timeOut = 15;

    /**
     * A map to store key-value pairs for temporary data storage.
     */
    static Map<String, String> storeMap = new HashMap<>();

    /**
     * The WebDriver instance.
     */
    static WebDriver driver;

    /**
     * The WebDriverWait instance for explicit waits.
     */
    WebDriverWait wait;

    /**
     * Constructor to initialize the WebDriver instance and WebDriverWait instance.
     */
    public CommonMethods() {
        driver = HelperClass.getDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(timeOut));
    }

    /**
     * Sets a key-value pair in the store map.
     *
     * @param key   The key to set.
     * @param value The value to set.
     */
    public void setValueToMap(String key, String value) {
        storeMap.put(key, value);
    }

    /**
     * Retrieves a value from the store map using the provided key.
     *
     * @param key The key to retrieve.
     * @return The value associated with the key.
     */
    public String getValueFromMap(String key) {
        return storeMap.get(key);
    }

    /**
     * Waits until the page is loaded completely.
     */
    public void waitForPageToLoad() {
        String pageLoadStatus;
        JavascriptExecutor js;
        do {
            js = (JavascriptExecutor) driver;
            pageLoadStatus = (String) js.executeScript("return document.readyState");
        } while (!pageLoadStatus.equals("complete"));
    }

    /**
     * Clicks on an element using Actions class.
     *
     * @param element The element to be clicked.
     * @throws Exception if an error occurs while clicking on the element
     */
    public void clickOnElementUsingActions(WebElement element) throws Exception {
        try {
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.click().perform();
        } catch (Exception e) {
            throw new Exception("Unable to click on the item", e);
        }
    }

    /**
     * Clicks on an element using JavaScript.
     *
     * @param element The element to be clicked.
     * @throws Exception if an error occurs while clicking on the element
     */
    public void clickOnElementUsingJs(WebElement element) throws Exception {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", element);
        } catch (Exception e) {
            throw new Exception("Unable to click on the item", e);
        }
    }

    /**
     * Returns the shadow element search context.
     *
     * @param shadowLocator The locator of the shadow element.
     * @return The shadow element search context.
     * @throws Exception if an error occurs while returning the shadow element
     */
    public SearchContext returnShadowElement(By shadowLocator) throws Exception {
        try {
            return driver.findElement(shadowLocator).getShadowRoot();
        } catch (Exception e) {
            throw new Exception("Unable to return shadow element", e);
        }
    }

    /**
     * Returns text from a shadow root element.
     *
     * @param shadowElementLocator The locator for the shadow root element.
     * @param targetLocator        The locator for the element whose text needs to be returned.
     * @return The text of the specified element within the shadow root.
     */
    public String returnTextFromTheShadowElement(By shadowElementLocator, By targetLocator) {
        return driver.findElement(shadowElementLocator).getShadowRoot().findElement(targetLocator).getText();
    }

    /**
     * Scrolls to view the element using JavaScript.
     *
     * @param element The element to scroll to.
     * @throws Exception if an error occurs while scrolling to the element
     */
    public static void javaScriptScrollToViewElement(WebElement element) throws Exception {
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        } catch (Exception e) {
            throw new Exception("Not able to scroll to the element", e);
        }
    }

    /**
     * Waits for an element to be visible.
     *
     * @param targetElement The element to be visible.
     * @return true if the element is visible, false otherwise
     * @throws Exception if an error occurs while waiting for the element to be visible
     */
    public boolean waitForVisibilityOfElement(WebElement targetElement) throws Exception {
        try {
            wait.until(ExpectedConditions.visibilityOf(targetElement));
            return true;
        } catch (TimeoutException e) {
            throw new Exception("Unable to locate the element", e);
        }
    }

    /**
     * Waits for an element to be visible using the given locator.
     *
     * @param targetElement The locator of the element to be visible.
     * @return true if the element is visible, false otherwise
     * @throws Exception if an error occurs while waiting for the element to be visible
     */
    public boolean waitForVisibilityOfBySelector(By targetElement) throws Exception {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(targetElement));
            return true;
        } catch (TimeoutException e) {
            throw new Exception("Unable to locate the element", e);
        }
    }

    /**
     * Updates the existing line with the provided string.
     *
     * @param existingLine  The existing line to update.
     * @param stringReplace The string to replace in the existing line.
     * @return The updated string.
     */
    public String locatorUpdate(String existingLine, String stringReplace) {
        return String.format(existingLine, stringReplace);
    }

    /**
     * Retrieves the text from the element located by the given locator.
     *
     * @param byLocator The locator of the element.
     * @return The text of the located element.
     * @throws Exception if an error occurs while retrieving the text from the element
     */
    public String getTextFromElement(By byLocator) throws Exception {
        try {
            return driver.findElement(byLocator).getText();
        } catch (Exception e) {
            throw new Exception("Unable to locate the element", e);
        }
    }

    /**
     * Retrieves a list of web elements based on the given locator.
     *
     * @param by The locator of the elements.
     * @return A list of web elements.
     * @throws Exception if an error occurs while retrieving the elements
     */
    public List<WebElement> getElementLists(By by) throws Exception {
        try {
            return driver.findElements(by);
        } catch (Exception e) {
            throw new Exception("Unable to return the elements", e);
        }
    }

    /**
     * Clicks on an element using JavaScript.
     *
     * @param by The locator of the element to be clicked.
     * @throws Exception if an error occurs while clicking on the element
     */
    public void clickOnElementBySelectorUsingJs(By by) throws Exception {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            WebElement element = driver.findElement(by);
            js.executeScript("arguments[0].click();", element);
        } catch (Exception e) {
            throw new Exception("Unable to click on the element", e);
        }
    }

    /**
     * Waits for an element to be clickable.
     *
     * @param targetElement The element to be clickable.
     * @throws TimeoutException if the element is not clickable within the timeout
     */
    public void waitForElementToBeClickable(WebElement targetElement) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(targetElement));
        } catch (TimeoutException e) {
            throw new TimeoutException();
        }
    }
}
