package com.swisscom.uiautomation.utils;

/**
 * This enum represents different contexts for storing data.
 * In this case, it is used to store product names.
 */
public enum Context {
    /**
     * Represents the context for storing product names.
     */
    PRODUCT_NAME;
}
