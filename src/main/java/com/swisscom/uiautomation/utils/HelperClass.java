/**
 * This class provides utility methods for managing WebDriver instances and opening web pages.
 */
package com.swisscom.uiautomation.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class HelperClass {

    private static HelperClass helperClass;
    private static WebDriver driver;
    public final static int TIMEOUT = 10;

    /**
     * Private constructor to initialize WebDriver instance with ChromeDriver.
     */
    private HelperClass() {
        WebDriverManager.chromedriver().setup();
        HelperClass.driver = new ChromeDriver();
        HelperClass.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(TIMEOUT));
        HelperClass.driver.manage().window().maximize();
    }

    /**
     * Opens the specified URL in the WebDriver instance.
     *
     * @param url The URL to open.
     */
    public static void openPage(String url) {
        driver.get(url);
    }

    /**
     * Returns the WebDriver instance.
     *
     * @return The WebDriver instance.
     */
    public static WebDriver getDriver() {
        return driver;
    }

    /**
     * Sets up the WebDriver instance.
     */
    public static void setUpDriver() {
        if (helperClass == null) {
            helperClass = new HelperClass();
        }
    }

    /**
     * Tears down the WebDriver instance.
     */
    public static void tearDown() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
        helperClass = null;
    }
}
