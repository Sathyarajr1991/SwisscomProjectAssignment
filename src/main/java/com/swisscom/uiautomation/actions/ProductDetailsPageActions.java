package com.swisscom.uiautomation.actions;

import com.swisscom.uiautomation.pages.ProductDetailsPage;
import org.junit.Assert;

/**
 * This class contains actions related to the product details page, such as user navigation and verification of notifications.
 */
public class ProductDetailsPageActions extends ProductDetailsPage {

    /**
     * Verifies if the user has successfully landed on the product details page.
     *
     * @throws Exception if an error occurs while verifying the landing on the product details page
     */
    public void userLandOnProductDetailsPage() throws Exception {
        Assert.assertTrue("Unable to locate the product details screen", isUserLandOnProductDetailsPage());
    }

    /**
     * Performs user action to add an item to the cart and verifies the notification displayed.
     *
     * @throws Exception if an error occurs while adding the item to the cart or verifying the notification
     */
    public void userAddedItemToTheCartAndVerifyTheNotification() throws Exception {
        Assert.assertEquals(NOTIFICATION_TEXT, isUserAddedItemToTheCart());
    }
}

