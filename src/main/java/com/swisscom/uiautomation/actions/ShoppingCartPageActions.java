package com.swisscom.uiautomation.actions;

import com.swisscom.uiautomation.pages.ShoppingCartPage;
import org.junit.Assert;

/**
 * This class contains actions related to the shopping cart page.
 */
public class ShoppingCartPageActions extends ShoppingCartPage {

    /**
     * Verifies if the user has landed on the shopping cart page.
     * @throws Exception if unable to locate cart title
     */
    public void userLandOnShoppingCartPage() throws Exception {
        Assert.assertTrue("Unable to locate cart title", isUserLandCartPage());
    }

    /**
     * Verifies the details of the saved product in the cart.
     * @throws Exception if unable to retrieve text from elements
     */
    public void userVerifySavedProductDetailsInTheCart() throws Exception {
        Assert.assertEquals(returnStoredProductText(), getTextFromElement(productNameInTheCart));
    }

    /**
     * Clicks on the cart trolley icon.
     * @throws Exception if unable to perform click action
     */
    public void userClickOnCartTrolleyIcon() throws Exception {
        clickOnCartTrolleyIcon();
    }
}
