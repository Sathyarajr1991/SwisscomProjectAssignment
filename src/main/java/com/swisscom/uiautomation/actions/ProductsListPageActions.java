package com.swisscom.uiautomation.actions;

import com.swisscom.uiautomation.pages.ProductsListPage;
import org.junit.Assert;

/**
 * This class contains actions related to the products list page, such as user navigation, interaction with filters, and verification of selected filters.
 */
public class ProductsListPageActions extends ProductsListPage {

    /**
     * Verifies if the user has successfully landed on the product list page with the specified title.
     *
     * @param productListTitleName The title of the product list page.
     * @throws Exception if an error occurs while verifying the landing on the product list page
     */
    public void userLandOnProductListPage(String productListTitleName) throws Exception {
        Assert.assertTrue("Unable to locate element in product list screen", isUserLandOnProductListPage(productListTitleName));
    }

    /**
     * Performs user actions to click on a filter type and select an option.
     *
     * @param filterTypeOptionName The name of the filter option to select.
     * @param filterType           The type of filter to click on.
     * @throws Exception if an error occurs while interacting with the filter
     */
    public void userClickOnFilterTypeAndSelectOption(String filterTypeOptionName, String filterType) throws Exception {
        clickOnFilterType(filterType);
        clickOnFilterTypeOption(filterTypeOptionName);
    }

    /**
     * Verifies if the specified filter is selected on the product list page.
     *
     * @param expFilterSelectedText The expected text of the selected filter.
     * @throws Exception if an error occurs while verifying the selected filter
     */
    public void userVerifyFilterIsSelected(String expFilterSelectedText) throws Exception {
        Assert.assertTrue("No such filter found", isFilterSelectedTextFound(expFilterSelectedText));
    }

    /**
     * Performs user action to navigate to the last product on the list and save its name.
     *
     * @throws Exception if an error occurs while navigating to the last product and saving its name
     */
    public void userGoToTheLastProductAndSaveProductName() throws Exception {
        storeTheLastProductName();
    }
}
