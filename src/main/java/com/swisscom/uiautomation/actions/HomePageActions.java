package com.swisscom.uiautomation.actions;

import com.swisscom.uiautomation.pages.HomePage;
import org.junit.Assert;

/**
 * This class contains actions related to the home page, such as user navigation and product selection.
 */
public class HomePageActions extends HomePage {

    /**
     * Verifies if the user has successfully landed on the homepage.
     *
     * @param url The URL of the homepage.
     * @throws Exception if an error occurs while verifying the landing on the homepage
     */
    public void userLandOnHomepage(String url) throws Exception {
        Assert.assertTrue("Unable to locate the home page locator", isUserOnHomePage(url));
    }

    /**
     * Performs user action to click on a navigation tab on the homepage.
     *
     * @param navigationTab The name of the navigation tab to click on.
     * @throws Exception if an error occurs while clicking on the navigation tab
     */
    public void userClickOnNavigationTab(String navigationTab) throws Exception {
        clickOnNavigationTab(navigationTab);
    }

    /**
     * Performs user action to select a product and its sub-product on the homepage.
     *
     * @param productCategorySelection The category of the product to select.
     * @param productionSelection      The specific product to select.
     * @throws Exception if an error occurs while selecting the product and its sub-product
     */
    public void userSelectProductAndSubProductSelection(String productCategorySelection, String productionSelection) throws Exception {
        clickOnProduct(productionSelection);
        clickOnSubProduct(productCategorySelection);
    }
}
