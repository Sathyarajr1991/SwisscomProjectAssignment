Feature: User add product to the cart

  Scenario: User Views Product, Adds It to Cart, and Verifies Details
    Given user is on the Swisscom homepage "https://www.swisscom.ch/en/residential.html"
    When user clicks on the "Devices" navigation tab
    And user clicks on "Headphones" under the "Accessories" products section
    Then the User verifies that they have landed on the "Earphones & headphones" page
    When user clicks on the "Apple" brand under the filter type "Brand"
    Then user verifies the "Apple" filter is selected
    When user goes to the last product, saves the item name and clicks on it
    Then user verifies the selected product title and adds the item to the cart
    When user clicks on the cart icon
    Then user verifies that they have landed on the cart screen
    And user verifies the product title in the cart with the previously stored item title
