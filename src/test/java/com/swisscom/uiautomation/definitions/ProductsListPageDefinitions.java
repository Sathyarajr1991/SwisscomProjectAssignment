/**
 * This class defines the step definitions related to the products list page.
 */
package com.swisscom.uiautomation.definitions;

import com.swisscom.uiautomation.actions.ProductsListPageActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class ProductsListPageDefinitions extends ProductsListPageActions {

    /**
     * Step definition to verify that the user is landed on the specified product list page.
     *
     * @param productListTitle The title of the product list page to verify.
     * @throws Exception if an error occurs during the verification process
     */
    @Then("the User verifies that they have landed on the {string} page")
    public void verifyUserIsLandedOnPage(String productListTitle) throws Exception {
        userLandOnProductListPage(productListTitle);
    }

    /**
     * Step definition to click on a brand under a filter type.
     *
     * @param filterType       The filter type.
     * @param filterTypeOption The option under the filter type to click.
     * @throws Exception if an error occurs while clicking on the brand under the filter type
     */
    @And("user clicks on the {string} brand under the filter type {string}")
    public void userClicksOnBrandUnderFilterType(String filterType, String filterTypeOption) throws Exception {
        userClickOnFilterTypeAndSelectOption(filterType, filterTypeOption);
    }

    /**
     * Step definition to verify that a filter is selected.
     *
     * @param selectedFilterName The name of the selected filter to verify.
     * @throws Exception if an error occurs during the verification process
     */
    @And("user verifies the {string} filter is selected")
    public void verifyUserIsSelectedFilter(String selectedFilterName) throws Exception {
        userVerifyFilterIsSelected(selectedFilterName);
    }

    /**
     * Step definition to go to the last product, save its name, and click on it.
     *
     * @throws Exception if an error occurs during the process
     */
    @And("user goes to the last product, saves the item name and clicks on it")
    public void userGoToTheLastProductAndSaveTheItemName() throws Exception {
        userGoToTheLastProductAndSaveProductName();
    }
}
