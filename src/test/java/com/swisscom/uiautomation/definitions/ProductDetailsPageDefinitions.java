/**
 * This class defines the step definitions related to the product details page.
 */
package com.swisscom.uiautomation.definitions;

import com.swisscom.uiautomation.actions.ProductDetailsPageActions;
import io.cucumber.java.en.And;

public class ProductDetailsPageDefinitions extends ProductDetailsPageActions {

    /**
     * Step definition to verify the product title and add the item to the cart.
     *
     * @throws Exception if an error occurs during the process
     */
    @And("user verifies the selected product title and adds the item to the cart")
    public void userGoToTheLastProductAndSaveTheItemName() throws Exception {
        userLandOnProductDetailsPage();
        userAddedItemToTheCartAndVerifyTheNotification();
    }
}
