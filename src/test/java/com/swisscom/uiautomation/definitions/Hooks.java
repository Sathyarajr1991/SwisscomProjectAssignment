/**
 * This class contains hooks to set up and tear down resources before and after scenario execution.
 */
package com.swisscom.uiautomation.definitions;

import com.swisscom.uiautomation.utils.HelperClass;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {

    /**
     * Sets up the WebDriver instance before scenario execution.
     */
    @Before
    public static void setUp() {
        HelperClass.setUpDriver();
    }

    /**
     * Tears down the WebDriver instance after scenario execution.
     * Takes a screenshot if the scenario has failed.
     *
     * @param scenario The scenario object representing the executed scenario.
     */
    @After
    public static void tearDown(Scenario scenario) {
        // Validate if scenario has failed
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) HelperClass.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
        }
        HelperClass.tearDown();
    }
}
