/**
 * This class defines the step definitions related to the shopping cart page.
 */
package com.swisscom.uiautomation.definitions;

import com.swisscom.uiautomation.actions.ShoppingCartPageActions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ShoppingCartPageDefinitions extends ShoppingCartPageActions {

    /**
     * Step definition to verify that the user is landed on the shopping cart page after clicking on the cart icon.
     *
     * @throws Exception if unable to perform actions related to clicking on the cart icon or verifying landing on the cart page
     */
    @Then("user verifies that they have landed on the cart screen")
    public void verifyUserIsLandedOnPage() throws Exception {
        userLandOnShoppingCartPage();
    }

    /**
     * Step definition to verify the product title in the cart with the previously stored item title.
     *
     * @throws Exception if unable to perform actions related to verifying product details in the cart
     */
    @Then("user verifies the product title in the cart with the previously stored item title")
    public void verifyTheProductDetailsInTheCartScreen() throws Exception {
        userVerifySavedProductDetailsInTheCart();
    }

    /**
     * Performs the action of clicking on the cart icon.
     * @throws Exception if there is an issue while clicking on the cart icon
     */
    @When("user clicks on the cart icon")
    public void userClicksOnTheCartIcon() throws Exception {
        userClickOnCartTrolleyIcon();
    }
}
