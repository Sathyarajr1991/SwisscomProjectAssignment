/**
 * This class defines the step definitions related to the Swisscom home page.
 */
package com.swisscom.uiautomation.definitions;

import com.swisscom.uiautomation.actions.HomePageActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class HomePageDefinitions extends HomePageActions {

    /**
     * Step definition to navigate to the Swisscom home page.
     *
     * @param url The URL of the Swisscom home page.
     * @throws Exception if an error occurs during navigation
     */
    @Given("user is on the Swisscom homepage {string}")
    public void user_is_on_swisscom_home_page(String url) throws Exception {
        userLandOnHomepage(url);
    }

    /**
     * Step definition to click on a navigation tab.
     *
     * @param navigationTab The name of the navigation tab to click.
     * @throws Exception if an error occurs while clicking on the navigation tab
     */
    @When("user clicks on the {string} navigation tab")
    public void user_clicks_on_navigation_tab(String navigationTab) throws Exception {
        userClickOnNavigationTab(navigationTab);
    }

    /**
     * Step definition to click on a product category from the product selection options.
     *
     * @param productsList          The product category to click.
     * @param productNavigationLink The navigation link under which the product category is located.
     * @throws Exception if an error occurs while clicking on the product category
     */
    @And("user clicks on {string} under the {string} products section")
    public void user_clicks_product_category_from_product_selection_option(String productsList, String productNavigationLink) throws Exception {
        userSelectProductAndSubProductSelection(productsList, productNavigationLink);
    }
}
