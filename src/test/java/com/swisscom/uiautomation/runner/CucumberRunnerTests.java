package com.swisscom.uiautomation.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

/**
 * This class serves as the entry point for executing Cucumber tests.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/", glue = "com.swisscom.uiautomation.definitions",
        plugin = {"pretty", "summary", "html:target/cucumber-html-report.html"},
        snippets = CAMELCASE)
public class CucumberRunnerTests {
}
